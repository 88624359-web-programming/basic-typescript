interface rectangle {
    width : number,
    height : number
}

interface coloredRectangle extends rectangle {
    color : string
}

const coloredRec : coloredRectangle = {
    color: "Blue",
    width: 20,
    height: 10
}

const rec : rectangle = {
    width: 20,
    height: 10
}


console.log(rec)
console.log(coloredRec)


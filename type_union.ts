function printStatusCode(code: number | string) {
    if (typeof code === 'string') {
        console.log('My status code is ' + code.toUpperCase() + ' type ' + typeof code);
    } else {
        console.log('My status code is ' + code + ' type ' + typeof code);
    }
}

printStatusCode(400);
printStatusCode('abc');
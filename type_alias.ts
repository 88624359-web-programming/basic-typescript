type carYear = number;
type carType = string;
type carModel = string;

type car = {
    year : carYear,
    type : carType,
    model : carModel
}

const CarYear : carYear = 2001;
const CarType : carType = "Toyota";
const CarModel : carModel = "Corolla";

const nissanJuke : car = {
    year : CarYear,
    type : "Nissan",
    model : "Juke"
}

console.log(nissanJuke)
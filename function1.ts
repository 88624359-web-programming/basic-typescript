function getTime() {
    return new Date().getTime();
}

console.log(getTime());

function printHello(): void {
    console.log("Hello");
}

printHello();

function mutiply(a: number, b: number):number {
    return a * b;
}

console.log(mutiply(1, 2));

function add(a:number, b:number, c?:number):number{
    return a + b + ( c || 0 );
}

console.log(add(1,2,3));
console.log(add(1,2));

function power(value:number, exponent:number = 10):number{
    return value ** exponent;
}

console.log(power(10))
console.log(power(10,2))

function divide({ divended, divisor } : { divended : number , divisor : number }) {
    return divended / divisor;
}

console.log(divide({divended : 100, divisor : 10}))

function add2(a: number, b:number , ...rest:number[]) {
    return a + b + rest.reduce((p,c) => p+c, 0);
}

console.log(add2(1,2,3,4,5))

type negate = ( value: number) => number;

const negatefunction : negate = (value: number) => value * -1;
const negatefunction2 : negate = function(value:number):number{
    return value*-1;
};

console.log(negatefunction(1))
console.log(negatefunction2(1))